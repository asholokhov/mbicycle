<?php

/**
 * One route from route collection.
 *
 * @author artem
 */
class CMBRoute {
    private $path;
    private $model_name;
    private $controller_name;
    private $action_name;
    private $params;
    
    public function __construct($path = "/", $model = '', $controller = "index", 
            $action = "index", $params = array()) {
        $this->path = $path; // todo: add validator
        $this->model_name = $model;
        $this->controller_name = $controller;
        $this->action_name = $action;
        $this->params = $params;
    }
    
    public function getPath() {
        return $this->path;
    }

    public function getRoute() {
        return array(
            $this->path => array (
                'model' => $this->model_name,
                'controller' => $this->controller_name,
                'action' => $this->action_name,
                'params' => $this->params
            )
        );
    }
}
