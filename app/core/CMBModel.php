<?php

/**
 * Base class for all models in application.
 *
 * @author artem
 */
abstract class CMBModel {
    abstract public function indexAction();
}
