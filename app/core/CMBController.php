<?php

/**
 * Description of CMBController
 *
 * @author artem
 */
abstract class CMBController {
    
    // default action for all controllers
    abstract public function index();
    
}
