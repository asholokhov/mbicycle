<?php

/**
 *  CMBRouter - provide routing for clean URL from routing map
 * and connect controller/model for routes which are not included
 * in routing map. 
 *
 * @since 0.0.1
 * @author artem
 */
class CMBRouter {
    private $routes;
    private $url_validator;

    public function redirect($url) {
        $this->routePath($url);
    }

    public function redirect404() {
        $this->routePath("/404/");
    }

    public function __construct($routeMap) {
        foreach ($routeMap as $route => $data) {
            $this->routes[$route] = $data;
        }
        $this->url_validator = new CMBUrlValidator();
    }

    public function addRoute(CMBRoute $route) {
        $this->routes[$route->getPath()] = $route->getRoute();
    }

    private function loadModule($model_name = '', $controller_name = 'home', 
            $action_name = 'index', $params_list = array())
    {
        // set controller and model prefix
        $controller_name = "Controller_" . $controller_name;
        $model_name = "Model_" . $model_name;
        $action_name = (empty($action_name) ? "index" : $action_name);
        
        // set 404 controller action
        if ($controller_name === "Controller_404") {
            $action_name = "index";
        }
        
        // add record to log
        CMBApplication::gate()->writeLog(__METHOD__, 
                "controller: $controller_name; "
              . "model: $model_name; "
              . "action: $action_name");
        
        // include model
        if (file_exists(MBROOT . '/app/models/' . $model_name . '.php')) {
            include MBROOT . '/app/models/' . $model_name . '.php';
        }

        // include controller
        if (file_exists(MBROOT . '/app/controllers/' . $controller_name . '.php')) {
            include MBROOT . '/app/controllers/' . $controller_name . '.php';
        } else {
            $this->redirect404();
            return;
        }

        // start
        $controller = new $controller_name;
        $action = strtolower($action_name);

        if (method_exists($controller, $action)) {
            $controller->$action($params_list);
        } else {
            $this->redirect404();
            return;
        }
    }

    private function routePath($url) {
        // write log
        CMBApplication::gate()->writeLog(__METHOD__, "Redirect to: $url");

        // validate url
        if (!$this->url_validator->validateLastSlash($url)) {
            $url .= "/";
        }
        
        // redirect to home
        if ($url == '/') {
            $this->loadModule();
            return;
        }

        // try to find route in routes map
        if ($this->routes != NULL && array_key_exists($url, $this->routes)) {
            $this->loadModule($this->routes[$url]['model'], 
                    $this->routes[$url]['controller'], 
                    $this->routes[$url]['action'], 
                    $this->routes[$url]['params']);
        } else {
            $request_parts = explode("/", $url);
            $controller_name = $request_parts[1];
            $action_name = $request_parts[2];
            
            array_splice($request_parts, 0, 3);
            $params_list = $request_parts;
            
            $this->loadModule('', $controller_name, $action_name, $params_list);
        }
    }

}
