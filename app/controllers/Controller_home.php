<?php

/**
 * Description of Controller_home
 *
 * @author artem
 */
class Controller_home extends CMBController {
    
    public function __construct() {
        // ...
    }

    public function index() {
        // we have no model, so just show 404 page
        $view_name = MBROOT.'/app/views/View_home.php';
        if (file_exists($view_name)) {
            include $view_name;
        } else {
            die("Cant find view for home page.");
        }
    }

}
