<?php

/**
 * Description of Controller_404
 *
 * @author artem
 */
class Controller_404 extends CMBController {
    
    public function __construct() {
        // ...
    }

    public function index() {
        // we have no model, so just show 404 page
        $view_name = MBROOT.'/app/views/View_404.php';
        if (file_exists($view_name)) {
            include $view_name;
        } else {
            die("Cant find view for 404 page.");
        }
    }

}
