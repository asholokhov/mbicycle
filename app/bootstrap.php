<?php

// path map
function get_autoload_dirs() {
    return array(
	'/app/core/',
	'/app/controllers/',
	'/app/models/',
	'/app/views/',
	'/protected/modules/',
        '/protected/validators/',
    );
}

// autoload magic function
function __autoload($class) {
    $file_name = $class.'.php';
    $inc_files = array();
    $path_map  = get_autoload_dirs();
    
    // find classes 
    foreach ($path_map as $include_path) {
	$inc_file_name = MBROOT.$include_path.$file_name;
	if (!in_array($inc_file_name, $inc_files) && file_exists($inc_file_name)) {
	    $inc_files[] = $inc_file_name;
	}
    }
    
    // load aviable classes
    foreach ($inc_files as $inc) {
	include $inc;
    }
}

// start application
CMBApplication::gate($config)->start();
