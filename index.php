<?php

// start session
session_start();

// define root folder
define('MBROOT', __DIR__);

// set timezone
date_default_timezone_set("Europe/Moscow");

// load configuration
include MBROOT . '/protected/config/conf.php';

// start bootstrap
include MBROOT . '/app/bootstrap.php';