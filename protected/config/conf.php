<?php

/**
 * Defines configuration for whole application.
 * 
 * @var array
 * @since 0.0.1
 */
$config = array(
    'debugMode' => true,
    'app_name' => 'Bicycle MVC Framevork',
    'app_description' => 'None',
    'version' => '0.0.1',
    
    'defaultRouting' => array(
	'/404/' => array (
            'model' => '',
	    'controller' => '404',
	    'action' => 'index', // index as default action
	    'params' => array()
	)
    ),
    
    'db' => array(
	'host' => 'localhost',
	'port' => 3306,
	'username' => 'root',
	'password' => ''
    ),
);