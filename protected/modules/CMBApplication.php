<?php

/**
 * Description of CMBApplication
 *
 * @author artem
 */
class CMBApplication {
    protected static $_instance;
    
    private $app_log;
    private $router;
    
    // constructing and singleton skeleton
    
    private function __construct($config) {
	// set debug mode
	if ($config['debugMode']) {
	    error_reporting(E_ALL);
	    ini_set('display_errors', 1);
	}
	
	// create log
	$this->app_log = new CMBLog($config['debugMode']);
	
	// create routing
	$this->router = new CMBRouter($config['defaultRouting']);
        
        // write log
        $this->writeLog(__METHOD__, "Start application");
    }
    
    private function __clone() {
        // ...
    }
    
    public static function gate($config = null) {
        if (self::$_instance === NULL) {
            if (isset($config)) {
                self::$_instance = new self($config);
            } else {
                // todo separate settings to class
            }
        }
        return self::$_instance;
    }
    
    // routing
    
    public function start() {
        $this->redirectTo($_SERVER['REQUEST_URI']);
    }
    
    public function redirectTo($url) {
        $this->router->redirect($url);
    }
    
    public function redirectTo404() {
        $this->router->redirect404();
    }
    
    // logger
    
    public function writeLog($class, $msg) {
        $this->app_log->log($class, $msg);
    }
    
}
