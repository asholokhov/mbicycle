<?php

/**
 * CMBLog - allow to write messages to log file in /app/logs/.
 * 
 * In debug mode all messages will be written in log-file.
 * In production mode, only exeption will be written.
 *
 * @author artem
 */
class CMBLog {
    private $debugMode;
    
    private $fileLink;
    private $logBuffer;
    
    private $logCounter;
    private $flushEvery;

    public function __construct($debugMode, $flushEvery = 3) {
	$this->debugMode = $debugMode;
        $this->logBuffer = "";
        $this->logCounter = 0;
        $this->flushEvery = $flushEvery;
        
        $file_name = "syslog_".date("d.m.Y").".log";
        $this->fileLink  = fopen(MBROOT."/app/logs/$file_name", "a+");
    }
    
    public function log($class, $msg, $debug = true) {
	if (!$this->debugMode) {
            return;
        }
        
        // E/D | class | time
        $DE = $debug ? "D" : "E";
        $this->logBuffer .= sprintf("[ %s | %-30s| %8s ]: %s\n", $DE, $class, date("H:i:s"), $msg);
        
        $this->logCounter++;
        if ($this->logCounter == $this->flushEvery) {
            $this->flush();
        }
    }
    
    public function __destruct() {
        $this->flush();
        fclose($this->fileLink);
    }

    public function flush() {
        fwrite($this->fileLink, $this->logBuffer);
        fflush($this->fileLink);
        
        $this->logCounter = 0;
        $this->logBuffer = "";
    }
    
}
