<?php

/**
 * Description of CMBUrlValidator
 *
 * @author artem
 */
class CMBUrlValidator {
    
    // return true if URL is valid for last slash
    // f.e: /f/e/ - valid url
    //      /f/e  - not valid
    public function validateLastSlash($url) {
        return substr($url, -1) == "/";	    
    }
    
}
